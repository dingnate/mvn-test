import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Test {
	public static void main(String[] args) throws InterruptedException {
		final ReentrantLock lock = new ReentrantLock();
		final Condition notEmpty = lock.newCondition();
		Thread t1 = new Thread() {
			public void run() {
				try {
					lock.lockInterruptibly();
					notEmpty.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					lock.unlock();
				}
			};
		};
		t1.start();
		Thread.sleep(1000);
		t1.interrupt();
	}
}