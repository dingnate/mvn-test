package com.concurrent;


public class Concurr {
	public static void _synchronized(String[] args) {
		Counter c = new Counter();
		for (int i = 0; i < 5; i++)
			if (i % 2 != 0) {
				new Thread() {
					public void run() {
						System.out.println(c.getValue());
						System.out.println(c.increment());
					};
				}.start();
			} else {
				new Thread() {
					public void run() {
						System.out.println(c.increment());
						System.out.println(c.getValue());
					};
				}.start();
			}
	}

	public static final class Counter {
		private long value = 0;

		public synchronized long getValue() {
			return value;
		}

		public synchronized long increment() {
			return ++value;
		}
	}
}