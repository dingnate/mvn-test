/**
 * 
 */
package com.svnkit.test;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.tmatesoft.svn.core.ISVNDirEntryHandler;
import org.tmatesoft.svn.core.ISVNLogEntryHandler;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.internal.util.SVNURLUtil;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.ISVNAnnotateHandler;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNLogClient;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNRevisionRange;
import org.tmatesoft.svn.core.wc.SVNWCClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;
import org.tmatesoft.svn.core.wc2.SvnFileKind;

/**
 * @author dinghn
 *
 */
public class SVNLogClientTest {

	private SVNLogClient logClient;
	private SVNURL svnUrl;;
	private String localRepoPath = "e:/works/projar";

	@Before
	public void before() {
		String user = "svnadmin";
		String passwd = "svnadmin";
		String url = "svn://127.0.0.1/projar";

		SVNClientManager manager = SVNClientManager.newInstance(
				SVNWCUtil.createDefaultOptions(true), user, passwd);
		logClient = manager.getLogClient();

		try {
			svnUrl = SVNURL.parseURIEncoded(url);
		} catch (SVNException e1) {
			e1.printStackTrace();
			return;
		}
	}

	// @Test
	public void doLog() throws SVNException {
		ISVNLogEntryHandler handler = new ISVNLogEntryHandler() {
			@Override
			public void handleLogEntry(SVNLogEntry logEntry)
					throws SVNException {
				System.out.println(logEntry);
			}
		};

		// 获取本地仓库日志
		logClient.doLog(new File[] { new File(localRepoPath) },
				SVNRevision.create(0), SVNRevision.HEAD, SVNRevision.HEAD,
				true, true, true, 20, new String[0], handler);

		// 获取url仓库日志
		Collection<SVNRevisionRange> revisionRanges = new ArrayList<SVNRevisionRange>();
		revisionRanges.add(new SVNRevisionRange(SVNRevision.create(0),
				SVNRevision.HEAD));
		logClient.doLog(svnUrl, new String[] {}, SVNRevision.HEAD,
				revisionRanges, true, true, true, 20, null, handler);
	}

	// @Test
	public void doList() throws SVNException {
		ISVNDirEntryHandler isvnDirEntryHandler = new ISVNDirEntryHandler() {
			@Override
			public void handleDirEntry(SVNDirEntry dirEntry)
					throws SVNException {
				System.out.println(dirEntry);
			}
		};
		// local仓库entry列表
		logClient.doList(new File(localRepoPath), SVNRevision.HEAD,
				SVNRevision.HEAD, true, SVNDepth.INFINITY,
				SVNDirEntry.DIRENT_ALL, isvnDirEntryHandler);
		// remote仓库entry列表
		logClient.doList(svnUrl, SVNRevision.HEAD, SVNRevision.HEAD, true,
				SVNDepth.INFINITY, SVNDirEntry.DIRENT_ALL, isvnDirEntryHandler);

	}

	@Test
	public void doAnnotate() throws SVNException {
		ISVNAnnotateHandler handler = new ISVNAnnotateHandler() {

			@Override
			public boolean handleRevision(Date date, long revision,
					String author, File contents) throws SVNException {
				return false;
			}

			@Override
			public void handleLine(Date date, long revision, String author,
					String line, Date mergedDate, long mergedRevision,
					String mergedAuthor, String mergedPath, int lineNumber)
					throws SVNException {
				// TODO Auto-generated method stub
				// implement this method as you wish, for example:
				System.out.println(revision + "  " + author + "  " + date
						+ "  " + mergedPath + "  " + line);

			}

			@Override
			public void handleLine(Date date, long revision, String author,
					String line) throws SVNException {
			}

			@Override
			public void handleEOF() throws SVNException {
			}
		};

		// lcoal
		String relative_file_path = "/trunk/testDir/b.txt";
		logClient.doAnnotate(new File(localRepoPath + relative_file_path),
				SVNRevision.HEAD, SVNRevision.create(0), SVNRevision.HEAD,
				false, true, handler, "UTF-8");
		// remote
		logClient.doAnnotate(svnUrl.appendPath(relative_file_path, false),
				SVNRevision.HEAD, SVNRevision.create(0), SVNRevision.HEAD,
				false, true, handler, "UTF-8");
	}

	public SVNLogClientTest() {
	}

}
