package com.svnkit.test;

import java.io.File;

import org.junit.Before;
import org.junit.Test;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNLogClient;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

public class SVNUpdateClientTest extends AbstractSVNTest {

	private SVNURL svnUrl;;
	private SVNUpdateClient updateClient;

	@Before
	public void before() {
		String user = "svnadmin";
		String passwd = "svnadmin";
		String url = "svn://127.0.0.1/projar";
		try {
			svnUrl = SVNURL.parseURIEncoded(url);
		} catch (SVNException e1) {
			e1.printStackTrace();
			return;
		}
		SVNClientManager manager = SVNClientManager.newInstance(
				SVNWCUtil.createDefaultOptions(true), user, passwd);
		updateClient = manager.getUpdateClient();
	}

	@Test
	public void doCheckout() throws SVNException {
		updateClient.doCheckout(svnUrl, new File("e:/svn/checkout/projar"),
				SVNRevision.HEAD, SVNRevision.HEAD, SVNDepth.INFINITY, true);
		
	}
	@Test
	public void doExport() throws SVNException {
		updateClient.doExport(svnUrl, new File("e:/svn/checkout/projar"), SVNRevision.HEAD, SVNRevision.HEAD, null, true, SVNDepth.INFINITY);
	}
	
	

	public SVNUpdateClientTest() {
	}

}
