/**
 * 
 */
package com.svnkit.test;

import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;

/**
 * @author dinghn
 *
 */
public abstract class AbstractSVNTest {
	static {
		SVNRepositoryFactoryImpl.setup();
		DAVRepositoryFactory.setup();
		FSRepositoryFactory.setup();
	}
	/**
	 * 
	 */
	public AbstractSVNTest() {
		// TODO Auto-generated constructor stub
	}

}
